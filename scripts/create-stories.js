const cheerio = require('cheerio');
const fetch = require('node-fetch');
const uniq = require('lodash/uniq');
const camelCase = require('lodash/camelCase');
const fs = require('fs');
const path = require('path');

const version = '4.5'
const baseUrl = `https://getbootstrap.com`;

(async () => {
  createStories('components', 'alerts');
  createStories('content', 'reboot');
})();

async function createStories (module, firstModule) {
  const url = `${baseUrl}/docs/${version}/${module}/${firstModule}`;
  const page = await fetch(url).then((res) => res.text());

  const $ = cheerio.load(page);

  const links = uniq($(`.bd-links a[href^="/docs/${version}/${module}"]`)
    .toArray()
    .map((a) => `${baseUrl}${a.attribs.href}`));

  const names = links.map(link => {
    const parts = link.split('/');
    return parts[parts.length - 2];
  });

  const pages = await Promise.all(links.map(link => fetch(link).then(res => res.text())))
  const templatePath = path.join(__dirname, '..', 'src', 'stories', 'generated');
  if (!fs.existsSync(templatePath)) {
    fs.mkdirSync(templatePath)
  }
  const template = fs.readFileSync(path.join(__dirname, '..', 'src', 'stories', 'story-template.txt'), {encoding: 'UTF8'})
  const itemTemplate = fs.readFileSync(path.join(__dirname, '..', 'src', 'stories', 'story-item-template.txt'), {encoding: 'UTF8'})

  pages.map((page, index) => {
    const dom$ = cheerio.load(page);

    const name = camelCase(names[index]);

    const codes = dom$('div.bd-example').toArray().map((element, i) => {
      return cheerio(element).html();
    });

    const stories = codes.map((code, i) => itemTemplate.replace('{{name}}', `${name}${i}`).replace('{{template}}', code)).join(' ');

    const story = template.replace('{{title}}', name).replace('{{stories}}', stories);

    fs.writeFileSync(path.join(__dirname, '..', 'src', 'stories', 'generated', `${name}.stories.ts`), story);
  })
}
