export default {
  title: 'code'
};

export const code0 = () => ({
  template: `
For example, <code>&lt;section&gt;</code> should be wrapped as inline.
`,
});
 export const code1 = () => ({
  template: `
<pre><code>&lt;p&gt;Sample text here...&lt;/p&gt;
&lt;p&gt;And another line of sample text here...&lt;/p&gt;
</code></pre>
`,
});
 export const code2 = () => ({
  template: `
<var>y</var> = <var>m</var><var>x</var> + <var>b</var>
`,
});
 export const code3 = () => ({
  template: `
To switch directories, type <kbd>cd</kbd> followed by the name of the directory.<br>
To edit settings, press <kbd><kbd>ctrl</kbd> + <kbd>,</kbd></kbd>
`,
});
 export const code4 = () => ({
  template: `
<samp>This text is meant to be treated as sample output from a computer program.</samp>
`,
});

