export default {
  title: 'alerts'
};

export const alerts0 = () => ({
  template: `

<div class="alert alert-primary" role="alert">
  A simple primary alert&#x2014;check it out!
</div>
<div class="alert alert-secondary" role="alert">
  A simple secondary alert&#x2014;check it out!
</div>
<div class="alert alert-success" role="alert">
  A simple success alert&#x2014;check it out!
</div>
<div class="alert alert-danger" role="alert">
  A simple danger alert&#x2014;check it out!
</div>
<div class="alert alert-warning" role="alert">
  A simple warning alert&#x2014;check it out!
</div>
<div class="alert alert-info" role="alert">
  A simple info alert&#x2014;check it out!
</div>
<div class="alert alert-light" role="alert">
  A simple light alert&#x2014;check it out!
</div>
<div class="alert alert-dark" role="alert">
  A simple dark alert&#x2014;check it out!
</div>
`,
});
 export const alerts1 = () => ({
  template: `

<div class="alert alert-primary" role="alert">
  A simple primary alert with <a href="#" class="alert-link">an example link</a>. Give it a click if you like.
</div>
<div class="alert alert-secondary" role="alert">
  A simple secondary alert with <a href="#" class="alert-link">an example link</a>. Give it a click if you like.
</div>
<div class="alert alert-success" role="alert">
  A simple success alert with <a href="#" class="alert-link">an example link</a>. Give it a click if you like.
</div>
<div class="alert alert-danger" role="alert">
  A simple danger alert with <a href="#" class="alert-link">an example link</a>. Give it a click if you like.
</div>
<div class="alert alert-warning" role="alert">
  A simple warning alert with <a href="#" class="alert-link">an example link</a>. Give it a click if you like.
</div>
<div class="alert alert-info" role="alert">
  A simple info alert with <a href="#" class="alert-link">an example link</a>. Give it a click if you like.
</div>
<div class="alert alert-light" role="alert">
  A simple light alert with <a href="#" class="alert-link">an example link</a>. Give it a click if you like.
</div>
<div class="alert alert-dark" role="alert">
  A simple dark alert with <a href="#" class="alert-link">an example link</a>. Give it a click if you like.
</div>
`,
});
 export const alerts2 = () => ({
  template: `
<div class="alert alert-success" role="alert">
  <h4 class="alert-heading">Well done!</h4>
  <p>Aww yeah, you successfully read this important alert message. This example text is going to run a bit longer so that you can see how spacing within an alert works with this kind of content.</p>
  <hr>
  <p class="mb-0">Whenever you need to, be sure to use margin utilities to keep things nice and tidy.</p>
</div>
`,
});
 export const alerts3 = () => ({
  template: `
<div class="alert alert-warning alert-dismissible fade show" role="alert">
  <strong>Holy guacamole!</strong> You should check in on some of those fields below.
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&#xD7;</span>
  </button>
</div>
`,
});

