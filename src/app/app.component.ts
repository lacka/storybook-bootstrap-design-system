import { Component } from '@angular/core';

@Component({
  selector: 'csx-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'storybook-bootstrap-design-system';
}
